/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasargod.airline.reservation.system.service;

/**
 *
 * @author ctlab
 */
public interface AirlineService {

    /**
     *
     * @param id
     * @param name
     * @param passportNo
     * @param dob
     * @param gender
     * @param email_Id
     * @return
     */
    public String saveAirline(Integer id,String name,String passportNo,java.util.Date dob,String gender,String email_Id);
}


