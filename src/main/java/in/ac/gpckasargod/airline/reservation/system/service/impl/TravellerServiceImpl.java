/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.airline.reservation.system.service.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ctlab
 */
public class TravellerServiceImpl extends ConnectionServiceImpl implements TravellerService{
    
    
    public String saveTravellerService(Integer id,String name,String passportno,java.util.Date dob,String gender,String emailid){
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO TRAVELLER_DETAILS (NAME,PASSPORTNO,DOB,GENDER,EMAILID) VALUES "+"('"+name+"','"+passportno+"','"+dob+"','"+gender+"','"+emailid+")";
            
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status !=1){
                return "Save failed";
            }else{
                return"Saved successfully";
            } 
            
            
            } catch (SQLException ex) {
            Logger.getLogger(TravellerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Save failed";
    }
    
    public Traveller readTraveller(Integer Id){
        Traveller traveller = null;
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String id = null;
            String query = "SELECT *FROM TRAVELLER WHERE ID=+id";
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                String Name = resultSet.getString("NAME");
                String PassportNo = resultSet.getString("PASSPORTNO");
                java.util.Date Dob = resultSet.getjava.util.Date("DOB");
                String Gender = resultSet.getString("GENDER");
                String EmailID = resultSet.getString("EMAILID");
                traveller = new traveller(Name,PassportNo,Dob,Gender,EmailID);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(TravellerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
   return traveller;
  }
}

    

    
    
    
