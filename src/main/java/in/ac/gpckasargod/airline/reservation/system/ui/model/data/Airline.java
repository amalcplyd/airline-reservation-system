/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.airline.reservation.system.ui.model.data;

import java.util.Date;

/**
 *
 * @author ctlab
 */
public class Airline {
    private Integer id;
    private String name;
    private String passportno;
    private String gender;
    private String emailid;       

    public Airline(Integer id, String name, String passportno, String gender, String emailid) {
        this.id = id;
        this.name = name;
        this.passportno = passportno;
        this.gender = gender;
        this.emailid = emailid;
    }

    public Airline(String Name, String PassportNo, Date Dob, String Gender, String EmailID) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassportno() {
        return passportno;
    }

    public void setPassportno(String passportno) {
        this.passportno = passportno;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }
    }    
    
    

